# Better City - mobile app

[![Build status](https://dev.azure.com/asajapin/ci-cd-pipelines/_apis/build/status/better%20city%20app)](https://dev.azure.com/asajapin/ci-cd-pipelines/_build/latest?definitionId=6)

Android app for reviews

## Contributing

- All platform-agnostic logic should reside in BetterCity.App project,
- all platform-specific logic should be wrapped as service and injected via platform-specific DI