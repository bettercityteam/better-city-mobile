using BetterCity.App.Helpers;
using BetterCity.SharedKernel.Model.Response;
using NUnit.Framework;
using System;

namespace BetterCity.App.Tests
{
    public class GeometryHelpersTests
    {
        [TestCase(2, 0.5, true)]
        [TestCase(0, 0, true)]
        [TestCase(2, 2, true)]
        [TestCase(4, 2, true)]
        [TestCase(1, 1, true)]
        [TestCase(1, 5, false)]
        public void TestLiesWithin(double lat, double lng, bool expected)
        {
            var polygon = new[] {
                new Point{Latitude = 0, Longitude = 0},
                new Point{Latitude = 0, Longitude = 2},
                new Point{Latitude = 2, Longitude = 2},
                new Point{Latitude = 2, Longitude = 4},
                new Point{Latitude = 4, Longitude = 4},
                new Point{Latitude = 4, Longitude = 1},
            };

            Assert.AreEqual(expected, GeometryRoutines.IsWithin(polygon)(new Model.RoutePoint(lat, lng, DateTimeOffset.Now)));
        }
    }
}