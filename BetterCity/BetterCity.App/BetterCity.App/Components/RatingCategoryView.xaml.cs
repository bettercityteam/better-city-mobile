﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace BetterCity.App.Components
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RatingCategoryView : ContentView
    {
        public RatingCategoryView(string name, int likes, int skips, int dislikes)
        {
            InitializeComponent();

            CategoryName.Text = name;

            var sum = likes + skips + dislikes;
            Value.Text = (likes * 1d / sum).ToString("P1");

            Positives.WidthRequest = Ratings.Width * likes / sum;
            Skipped.WidthRequest = Ratings.Width * skips / sum;
            Positives.WidthRequest = Ratings.Width * dislikes / sum;
        }
    }
}