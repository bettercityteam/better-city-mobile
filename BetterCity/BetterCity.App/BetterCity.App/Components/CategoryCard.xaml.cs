﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace BetterCity.App.Components
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CategoryCard : ContentView
    {
        public string CategoryId;
        
        public CategoryCard()
        {
            InitializeComponent();
            
            DislikeButton.Source = ImageSource.FromResource("BetterCity.App.Resources.dislike.png");
            LikeButton.Source = ImageSource.FromResource("BetterCity.App.Resources.like.png");
            SkipButton.Source = ImageSource.FromResource("BetterCity.App.Resources.skip.png");
        }

        public event Action<CategoryCard, string, bool?> DecisionMade;
        public event Action<CategoryCard, string> TextSubmitted;
        public bool IsPicMode = true;

        public void Load(string id, byte[] image, string text)
        {
            try
            {
                this.CategoryId = id;
                CategoryImage.Source = ImageSource.FromStream(() => new MemoryStream(image));
                CategoryText.Text = text;
            }
            catch (Exception e)
            {
            }
        }

        private void OnLike(object sender, EventArgs e)
        {
            DecisionMade?.Invoke(this, CategoryId, true);
        }

        private void OnSkip(object sender, EventArgs e)
        {
            DecisionMade?.Invoke(this, CategoryId, null);
        }

        private void OnDislike(object sender, EventArgs e)
        {
            DecisionMade?.Invoke(this, CategoryId, false);
        }

        public void LoadForText()
        {
            text.IsVisible = true;
            pic.IsVisible = false;
            IsPicMode = false;
        }

        private void OnTextSubmit(object sender, EventArgs e)
        {
            TextSubmitted?.Invoke(this, TextReview.Text);
            text.IsVisible = false;
            pic.IsVisible = true;
            IsPicMode = false;
        }

        public string GetReviewText() => TextReview.Text;
    }
}