﻿using System.Linq;
using System.Threading.Tasks;
using BetterCity.App.Model;
using BetterCity.SharedKernel.Contracts;
using BetterCity.SharedKernel.Model.Request;
using BetterCity.SharedKernel.Model.Response;
using Xamarin.Essentials;
using static BetterCity.SharedKernel.Constants.Endpoints;

namespace BetterCity.App.Services
{
    public class ApiClient : MobileClientTargetedApi, MobileClientTargetedAuthApi
    {
        private readonly HttpCaller caller;
        private readonly SecretsStore secrets;

        public ApiClient(HttpCaller caller, SecretsStore secrets)
        {
            this.caller = caller;
            this.secrets = secrets;
        }

        public bool IsAuthenticated
        {
            get
            {
                var strings = secrets.GetAuthCookie().GetAwaiter().GetResult();
                return !(strings is null) && strings.Length != 0 && strings.Any(x => !string.IsNullOrWhiteSpace(x));
            }
        }

        private static string Combine(params string[] segments) {
            return string.Join("/", segments.Select((s, i) => { 
                if (i == 0) return s.TrimEnd('/');
                if (i == segments.Length-1) return s.TrimStart('/');
                return s.Trim('/');
            }));
        }

        public async Task<ReviewCategory[]> GetReviewCategories(string placeId, string[] waypointIds)
        {
            await Task.Delay(1500);
            return Stubs.Responses.GetCategories;
            return await caller.ForUrl(Combine(Api.V1.BaseUrl, Api.V1.GetReviewCategories))
                .WithBody(new SharedKernel.Model.Request.ReviewCategories {PlaceId = placeId, Waypoints = waypointIds})
                .WithHeader("Cookie", await secrets.GetAuthCookie())
                .PostAsync<ReviewCategory[]>();
        }

        public async Task<PlaceGeometry[]> GetPlacesInArea(double latitude, double longitude, double radiusKm)
        {
            return Stubs.Responses.GetPlaces;
            return await caller.ForUrl(Combine(Api.V1.BaseUrl, Api.V1.GetPlacesInArea))
                            .WithQueryParameter("lat", latitude)
                            .WithQueryParameter("lng", longitude)
                            .WithQueryParameter("rad", radiusKm)
                            .WithHeader("Cookie", await secrets.GetAuthCookie())
                            .GetAsync<PlaceGeometry[]>();
        }

        public async Task<bool> RequestCode(string phone)
        {
            return Stubs.Responses.RequestCode;
            return await caller.ForUrl(Combine(Auth.V1.BaseUrl, Auth.V1.RequestCodeUrl))
                .WithBody(phone)
                .PostAsync<bool>();
        }

        public async Task<bool> SignIn(string phone, string code)
        {
            await secrets.SetAuthCookie(new[] {"s"});
            return Stubs.Responses.SignIn;
            return await caller.ForUrl(Combine(Auth.V1.BaseUrl, Auth.V1.SignIn))
                .WithBody(new SharedKernel.Model.Request.ClientSignIn {Phone = phone, Code = code})
                .WithResponseProcessor(async m =>
                {
                    if (m.IsSuccessStatusCode)
                        await secrets.SetAuthCookie(m.Headers.GetValues("Set-Cookie"));
                })
                .PostAsync<bool>();
        }

        public async Task<bool> CreateReview(NewReview review)
        {
            return Stubs.Responses.CreateReview;
            return await caller.ForUrl(Combine(Api.V1.BaseUrl, Api.V1.CreateReview))
                .WithHeader("Cookie", await secrets.GetAuthCookie())
                .WithBody(review)
                .PostAsync<bool>();
        }

        public async Task<Review[]> GetOwnReviews(string placeId)
        {
            return Stubs.Responses.GetOwnReviews;
            return await caller.ForUrl(Combine(Api.V1.BaseUrl, Api.V1.GetOwnReviews, placeId))
                .WithHeader("Cookie", await secrets.GetAuthCookie())
                .GetAsync<Review[]>();
        }

        public async Task<PlaceInfo> GetPlace(string placeId)
        {
            return Stubs.Responses.GetPlace;
            return await caller.ForUrl(Combine(Api.V1.BaseUrl, Api.V1.GetPlace, placeId))
                .WithHeader("Cookie", await secrets.GetAuthCookie())
                .GetAsync<PlaceInfo>();
        }

        public async Task<byte[]> GetImage(string id)
        {
            await Task.Delay(1000);
            return Stubs.Responses.GetImage(id);
            return await caller.ForUrl(Combine(Api.V1.BaseUrl, Api.V1.GetImage, id))
                .WithHeader("Cookie", await secrets.GetAuthCookie())
                .GetAsync<byte[]>();
        }
    }
}
