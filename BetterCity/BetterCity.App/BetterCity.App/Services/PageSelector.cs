﻿using System;
using System.Collections.Generic;
using BetterCity.App.Pages;
using Xamarin.Forms;

namespace BetterCity.App.Services
{
    public class PageSelector
    {
        public const string
            PageSelectionKey = "page-selection-intent",
            LaunchReview = nameof(NewReview),
            RequestId = "requestId";

        private readonly ApiClient client;

        public PageSelector(ApiClient client)
        {
            this.client = client;
        }

        public Page GetPage(Dictionary<string, string> data)
        {
            Page ret;
            if (!client.IsAuthenticated) ret = new Auth(data);
            if (data is null || !data.TryGetValue(PageSelectionKey, out var value)) ret = new Home();
            else switch (value)
            {
                case LaunchReview:
                    ret = new NewReview(data);
                    break;
                default: ret = new Home();
                    break;
            }

            NavigationPage.SetHasNavigationBar(ret, false);
            return ret;
        }
    }
}