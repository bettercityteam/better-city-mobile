﻿using System.IO;
using System.Linq;
using BetterCity.SharedKernel.Model.Response;

namespace BetterCity.App.Services.Stubs
{
    public static class Responses
    {
        public static bool RequestCode = true, SignIn = true, CreateReview = true;

        public static PlaceGeometry[] GetPlaces = new[]
        {
            new PlaceGeometry
            {
                Name = "home",
                BoundingPolygon = new[]
                {
                    new Point {Latitude = 51.75, Longitude = 55.04},
                    new Point {Latitude = 51.75, Longitude = 55.08},
                    new Point {Latitude = 51.79, Longitude = 55.08},
                    new Point {Latitude = 51.79, Longitude = 55.04},
                },
                // Summary = "none",
                PlaceId = "123",
                Waypoints = new[]
                {
                    new Waypoint
                    {
                        Latitude = 51.778142,
                        Longitude = 55.062904,
                        Name = "wp1",
                        RadiusMeters = 40
                    }
                }
            }
        };

        private static byte[] ReadResource(string name)
        {
            var assembly = typeof(Responses).Assembly;
            var s = assembly.GetManifestResourceStream(
                assembly.GetManifestResourceNames().FirstOrDefault(x => x.EndsWith(name)));
            using var ms = new MemoryStream();
            s?.CopyTo(ms);
            return ms.ToArray();
        } 
        
        public static ReviewCategory[] GetCategories = new[]
        {
            new ReviewCategory{Id = "1", Name = "cat 1", Image = ReadResource("pic2.jpeg")},
            new ReviewCategory{Id = "2", Name = "cat 2", Image = ReadResource("pic4.jpeg")},
            new ReviewCategory{Id = "3", Name = "cat 3", Image = ReadResource("pic6.jpeg")},
            new ReviewCategory{Id = "4", Name = "cat 4", Image = ReadResource("pic8.jpeg")},
            new ReviewCategory{Id = "5", Name = "cat 5", Image = ReadResource("pic10.jpeg")},
        };

        public static PlaceInfo GetPlace = 
            new PlaceInfo()
            {
                Name = "home",
                PlaceId = "123",
                Summary = "none provided",
                ImageFiles = new []{"1","2", "3", "4"},
                Rating = 0.75,
                CategoryRatings = new []
                {
                    new CategoryRating{Name = "category 1", Skipped = 2, NegativeReviews = 4, PositiveReviews = 5},
                    new CategoryRating{Name = "category 2", Skipped = 1, NegativeReviews = 5, PositiveReviews = 9},
                    new CategoryRating{Name = "category 3", Skipped = 4, NegativeReviews = 3, PositiveReviews = 8},
                    new CategoryRating{Name = "category 4", Skipped = 3, NegativeReviews = 7, PositiveReviews = 5},
                }
        };

        public static Review[] GetOwnReviews =
        {
            new Review()
            {
                Id = "123",
                Text = "sample text",
                PlaceId = "123",
                DislikedCategoriesNames = new []{"cat 1","cat 2"},
                LikedCategoriesNames = new []{"cat 3", "cat 4"},
                SkippedCategoriesNames = new []{"cat 5"}
            }
        };

        public static byte[] GetImage(string id)
        {
            return ReadResource($"pic{id}.jpeg");
        }
    }
}