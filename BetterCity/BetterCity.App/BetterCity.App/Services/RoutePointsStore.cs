﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BetterCity.App.DAL;
using BetterCity.App.Model;

namespace BetterCity.App.Services
{
    public class RoutePointsStore
    {
        private readonly Func<DataContext> storeFactory;

        public RoutePointsStore(Func<DataContext> storeFactory)
        {
            this.storeFactory = storeFactory;
        }

        private readonly List<RoutePoint> pointsCache = new List<RoutePoint>();

        public void AddPoint(double lat, double lng, DateTimeOffset timestamp)
        {
            var point = new RoutePoint(lat, lng, timestamp);
            // todo : check if too close to other points and can be skipped
            // WritePoint(point);
            pointsCache.Add(point);
            InvokePointAdded(point);
        }
        public IReadOnlyList<RoutePoint> GetPoints() => pointsCache.AsReadOnly();
        public event Action<IReadOnlyList<RoutePoint>, RoutePoint> PointAdded;

        private void InvokePointAdded(RoutePoint point)
        {
            PointAdded?.Invoke(pointsCache.ToList(), point);
        }

        public void Remove(IEnumerable<RoutePoint> points)
        {
            foreach (var p in points)
                pointsCache.Remove(p);
            // RemovePoints(points);
        }

        private void WritePoint(RoutePoint point)
        {
            using (var context = storeFactory())
            {
                context.Points.Add(point);
                context.SaveChanges();
            }
        }

        private void RemovePoints(IEnumerable<RoutePoint> points)
        {
            using (var context = storeFactory())
            {
                context.Points.RemoveRange(points);
                context.SaveChanges();
            }
        }
    }
}
