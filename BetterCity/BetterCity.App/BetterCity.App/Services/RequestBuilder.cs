﻿using Microsoft.AspNetCore.WebUtilities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using BetterCity.App.Helpers;

namespace BetterCity.App.Services
{
    public class RequestBuilder
    {
        private string url;
        private readonly HttpCaller httpCaller;
        private string body;
        private Dictionary<string, string[]> headers = new Dictionary<string, string[]>();
        private Func<HttpResponseMessage, Task> processor;

        public RequestBuilder(string url, HttpCaller httpCaller)
        {
            this.url = url;
            this.httpCaller = httpCaller;
        }

        public RequestBuilder WithQueryParameter(string name, object value)
        {
            url = QueryHelpers.AddQueryString(url, name, value is string s ? s : JsonConvert.SerializeObject(value));
            return this;
        }

        public RequestBuilder WithHeader(string name, params string[] value)
        {
            headers[name] = value;
            return this;
        }

        public RequestBuilder WithBody(object value)
        {
            body = JsonConvert.SerializeObject(value);
            return this;
        }

        private HttpRequestMessage ToMessage(HttpMethod method)
        {
            var message = new HttpRequestMessage(method, url);
            foreach (var (key, value) in headers)
                message.Headers.Add(key, value);
            if (!string.IsNullOrWhiteSpace(body))
                message.Content = new StringContent(body);
            return message;
        }

        public RequestBuilder WithResponseProcessor(Func<HttpResponseMessage, Task> processor)
        {
            this.processor = processor;
            return this;
        }

        public Task<T> GetAsync<T>() => httpCaller.CallAsync<T>(ToMessage(HttpMethod.Get), processor);
        public Task<T> PostAsync<T>() => httpCaller.CallAsync<T>(ToMessage(HttpMethod.Post), processor);
    }
}
