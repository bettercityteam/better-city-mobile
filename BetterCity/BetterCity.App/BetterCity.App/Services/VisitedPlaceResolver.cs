﻿using System;
using System.Collections.Generic;
using BetterCity.App.Helpers;
using System.Linq;
using BetterCity.App.Model;
using BetterCity.SharedKernel.Model.Response;
using Xamarin.Forms;

namespace BetterCity.App.Services
{
    public class VisitedPlaceResolver
    {
        private readonly TimeSpan timeout;
        private readonly ApiClient client;
        private readonly AppSettings settings;
        private readonly RoutePointsStore store;

        public VisitedPlaceResolver(RoutePointsStore store, ApiClient client, AppSettings settings)
        {
            store.PointAdded += OnPointAdded;
            this.store = store;
            this.client = client;
            this.settings = settings;
            timeout = TimeSpan.FromSeconds(settings.PlaceResolvingIntervalSeconds);
        }

        private async void OnPointAdded(IReadOnlyList<RoutePoint> points, RoutePoint point)
        {
            if (points is null) return;
            if (points.Count > 1 && points[points.Count - 1].Timestamp - points[0].Timestamp > timeout)
            {
                var (lat, lng, rad) = GeometryRoutines.GetContainingArea(points);
                var places = await client.GetPlacesInArea(lat, lng, rad);
                var visitedPlaces = places.Where(p =>
                {
                    var isWithinBounds = GeometryRoutines.IsWithin(p.BoundingPolygon);
                    return points.Count(isWithinBounds) >= settings.MinimumPointsInbound;
                }).ToArray();
                PlacesFound?.Invoke(visitedPlaces.Select(p =>
                {
                    var wps = p.Waypoints.Where(w => points.Any(GeometryRoutines.IsWithin(w))).ToArray();
                    return (p, wps);
                }).ToArray());
                store.Remove(points);
            }
        }

        public event Action<(PlaceGeometry, Waypoint[])[]> PlacesFound;
    }
}
