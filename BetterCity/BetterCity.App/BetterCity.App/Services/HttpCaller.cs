﻿using System;
using Microsoft.AspNetCore.WebUtilities;
using Newtonsoft.Json;
using System.Net.Http;
using System.Threading.Tasks;

namespace BetterCity.App.Services
{
    public class HttpCaller
    {
        private readonly HttpClient client;

        public HttpCaller(HttpClient client)
        {
            this.client = client;
        }

        public RequestBuilder ForUrl(string url) { return new RequestBuilder(url, this); }

        public async Task<T> CallAsync<T>(HttpRequestMessage message, Func<HttpResponseMessage, Task> processor)
        {
            var response = await client.SendAsync(message);
            var content = await response.Content.ReadAsStringAsync();
            if (!(processor is null))
                await processor(response);
            if (response.IsSuccessStatusCode is T t) return t;
            if (response.IsSuccessStatusCode)
                return JsonConvert.DeserializeObject<T>(content);
            throw new HttpRequestException(content);
        }
    }
}
