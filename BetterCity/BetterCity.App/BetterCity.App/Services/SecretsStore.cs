﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Xamarin.Essentials;

namespace BetterCity.App.Services
{
    public class SecretsStore
    {
        private readonly string 
            JwtAuth = "better-city-jwt-auth", 
            JwtReissue = "better-city-jwt-reissue",
            AuthCookie = "better-city-auth-cookie";

        public async Task<string[]> GetAuthCookie()
        {
            return (await SecureStorage.GetAsync(AuthCookie))?.Split('\n') ?? new string[0];
        }

        public Task<string> GetJwtAuthToken()
        {
            return SecureStorage.GetAsync(JwtAuth);
        }

        public Task<string> GetJwtUpdateToken()
        {
            return SecureStorage.GetAsync(JwtReissue);
        }

        public Task SetAuthCookie(IEnumerable<string> cookies)
        {
            return SecureStorage.SetAsync(AuthCookie, string.Join("\n", cookies));
        }
    }
}
