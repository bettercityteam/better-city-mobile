﻿using BetterCity.App.Services;
using System.Collections.Generic;
using System.Text;

namespace BetterCity.App.Helpers
{
    public static class Extensions
    {
        public static void Deconstruct<K, V>(this KeyValuePair<K, V> pair, out K key, out V value)
        {
            key = pair.Key;
            value = pair.Value;
        }
    }
}
