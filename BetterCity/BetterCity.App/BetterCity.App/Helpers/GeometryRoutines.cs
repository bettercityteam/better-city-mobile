﻿using System;
using System.Collections.Generic;
using BetterCity.App.Model;
using BetterCity.SharedKernel.Model.Response;

namespace BetterCity.App.Helpers
{
    public static class GeometryRoutines
    {
        public static (double lat, double lng, double rad) GetContainingArea(IReadOnlyList<RoutePoint> points)
        {
            double latMax = 0, latMin = 180, lngMax = 0, lngMin = 180;
            foreach (var point in points)
            {
                if (latMax < point.Latitude) latMax = point.Latitude;
                if (lngMax < point.Longitude) lngMax = point.Longitude;
                if (latMin > point.Latitude) latMin = point.Latitude;
                if (lngMin > point.Longitude) lngMin = point.Longitude;
            }

            double s1 = latMax - latMin, s2 = lngMax - lngMin;
            double lat = (latMin + latMax) / 2, lng = (lngMin + lngMax) / 2, rad = Math.Sqrt(s1 * s1 + s2 * s2) / 2;
            return (lat, lng, rad);
        }

        public static double DistanceMeters(double latitude1, double longitude1, double latitude2, double longitude2)
        {
            const double r = 6378.137;
            var dLat = latitude2 * Math.PI / 180 - latitude1 * Math.PI / 180;
            var dLon = longitude2 * Math.PI / 180 - longitude1 * Math.PI / 180;
            var a = Math.Sin(dLat / 2) * Math.Sin(dLat / 2) +
                    Math.Cos(latitude1 * Math.PI / 180) * Math.Cos(latitude2 * Math.PI / 180) *
                    Math.Sin(dLon / 2) * Math.Sin(dLon / 2);
            var c = 2 * Math.Atan2(Math.Sqrt(a), Math.Sqrt(1 - a));
            var d = r * c;
            return d * 1000;
        }

        public static Func<RoutePoint, bool> IsWithin(Waypoint w)
        {
            return p => DistanceMeters(p.Latitude, p.Longitude, w.Latitude, w.Longitude) < w.RadiusMeters;
        }

        public static Func<RoutePoint, bool> IsWithin(Point[] bound)
        {
            return point =>
            {
                double x = point.Latitude, y = point.Longitude;

                var inside = false;
                for (int i = 0, j = bound.Length - 1; i < bound.Length; i++)
                {
                    double xi = bound[i].Latitude, yi = bound[i].Longitude;
                    double xj = bound[j].Latitude, yj = bound[j].Longitude;

                    var intersect = ((yi > y) != (yj > y))
                                    && (x < (xj - xi) * (y - yi) / (yj - yi) + xi);
                    if (intersect) inside = !inside;
                    j = i;
                }

                return inside;
            };
        }
    }
}