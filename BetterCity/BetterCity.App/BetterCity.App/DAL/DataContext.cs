﻿using BetterCity.App.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace BetterCity.App.DAL
{
    public class DataContext : DbContext {
        private readonly string dbName = "store.db";

        public DataContext(string name = null)
        {
            dbName = name ?? dbName;   
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite($"Data Source={dbName}");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            RoutePoint.ConfigureForEf(modelBuilder.Entity<RoutePoint>());
            ReviewRequest.ConfigureForEf(modelBuilder.Entity<ReviewRequest>());
                
            base.OnModelCreating(modelBuilder);
        }

        public DbSet<RoutePoint> Points { get; set; }
        public DbSet<ReviewRequest> Requests { get; set; }
    }
}
