﻿using System.Collections.Generic;
using System.Text;

namespace BetterCity.App.Model
{
    public class AppSettings
    {
        public string ServiceUrl { get; set; }
        public int MinimumPointsInbound { get; set; }
        public double PollIntervalSeconds { get; set; }
        public double PlaceResolvingIntervalSeconds { get; internal set; }
    }
}
