﻿using BetterCity.App.DAL;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Globalization;

namespace BetterCity.App.Model
{
    public class RoutePoint
    {
        public RoutePoint(double latitude, double longitude, DateTimeOffset timestamp)
        {
            Latitude = latitude;
            Longitude = longitude;
            Timestamp = timestamp;
        }

        public double Latitude { get; }
        public double Longitude { get; }
        public DateTimeOffset Timestamp { get; }

        public static void ConfigureForEf(EntityTypeBuilder<RoutePoint> builder)
        {
            builder
                .Property(x => x.Latitude)
                .IsRequired()
                .HasColumnType("REAL");
            builder
                .Property(x => x.Longitude)
                .IsRequired()
                .HasColumnType("REAL");
            builder
                .Property(x => x.Timestamp)
                .IsRequired()
                .HasColumnType("TEXT")
                .HasConversion(dto => dto.ToString("O"), s => DateTimeOffset.ParseExact(s, "O", CultureInfo.InvariantCulture));
            builder
                .ToTable(nameof(RoutePoint))
                .HasKey(x => new { x.Latitude, x.Longitude, x.Timestamp });
        }
    }
}
