﻿using System;
using System.Globalization;
using System.Linq;
using BetterCity.SharedKernel.Model.Response;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace BetterCity.App.Model
{
    public class ReviewRequest
    {
        public string PlaceId { get; set; }
        public DateTimeOffset Timestamp { get; set; }
        public DateTimeOffset MutedTill { get; set; }
        public string Name { get; set; }
        public string[] Waypoints { get; set; }


        public static void ConfigureForEf(EntityTypeBuilder<ReviewRequest> builder)
        {
            builder
                .Property(x => x.PlaceId)
                .IsRequired()
                .HasColumnType("TEXT");
            builder
                .Property(x => x.Timestamp)
                .IsRequired()
                .HasColumnType("TEXT")
                .HasConversion(dto => dto.ToString("O"), s => DateTimeOffset.ParseExact(s, "O", CultureInfo.InvariantCulture));
            builder
                .Property(x => x.MutedTill)
                .IsRequired()
                .HasColumnType("TEXT")
                .HasConversion(dto => dto.ToString("O"), s => DateTimeOffset.ParseExact(s, "O", CultureInfo.InvariantCulture));
            builder
                .Property(x => x.Name)
                .IsRequired()
                .HasColumnType("TEXT");
            builder
                .Property(x => x.Waypoints)
                .IsRequired()
                .HasColumnType("TEXT")
                .HasConversion(ws => string.Join(",", ws), s => s.Split(',').ToArray());
            builder
                .ToTable(nameof(ReviewRequest))
                .HasKey(x => x.PlaceId);
        }

        public static ReviewRequest FromPlaceAndWaypoints((PlaceGeometry, Waypoint[]) pair)
        {
            var (place, waypoints) = pair;
            return new ReviewRequest { 
                PlaceId = place.PlaceId,
                Timestamp = DateTimeOffset.Now,
                MutedTill = DateTimeOffset.MinValue,
                Name = place.Name, 
                Waypoints = waypoints.Select(x => x.Id).ToArray()
            };
        }
    }
}