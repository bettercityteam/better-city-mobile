﻿using BetterCity.App.DAL;
using BetterCity.App.Model;
using BetterCity.App.Services;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using static BetterCity.SharedKernel.Constants.Endpoints;

namespace BetterCity.App.DI
{
    public static class Dependencies
    {
        private static readonly IServiceCollection services;
        private static IServiceProvider provider;

        static Dependencies()
        {
            services = new ServiceCollection();

            var assembly = typeof(Dependencies).Assembly;
            var config = new ConfigurationBuilder()
                .AddJsonStream(assembly.GetManifestResourceStream(assembly.GetManifestResourceNames().First(x => x.EndsWith("appsettings.json"))))
                .Build();
            var settings = config.Get<AppSettings>();

            services.AddSingleton<IConfiguration>(config);
            services.AddSingleton(settings);
            services.AddSingleton<RoutePointsStore>();
            services.AddSingleton<SecretsStore>();
            services.AddTransient<ApiClient>();
            services.AddTransient<HttpCaller>();
            services.AddTransient(isp => new HttpClient { BaseAddress = new Uri(settings.ServiceUrl) });
            services.AddSingleton<VisitedPlaceResolver>();
            services.AddSingleton<Func<DataContext>>(() =>
            {
                var dataContext = new DataContext(
                    Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData),
                        "store.db3"));
                dataContext.Database.EnsureCreated();
                return dataContext;
            });
            services.AddSingleton<PageSelector>();

            provider = services.BuildServiceProvider();
        }

        public static void Register(Action<IServiceCollection> registration)
        {
            registration?.Invoke(services);
            provider = services.BuildServiceProvider();
        }

        public static T Get<T>()
        {
            if (provider is null) throw new ArgumentNullException(nameof(provider), "provider is not built");
            return provider.GetService<T>();
        }
    }
}
