﻿using System;
using System.Collections.Generic;
using BetterCity.App.DI;
using BetterCity.App.Services;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace BetterCity.App
{
    public partial class App : Application
    {
        public App(Dictionary<string, string> data)
        {
            InitializeComponent();

            // todo : remove debug code
            var secrets = Dependencies.Get<SecretsStore>();
            secrets.SetAuthCookie(new string[0]);
            // end
            
            var selector = Dependencies.Get<PageSelector>();

            MainPage = new NavigationPage(selector.GetPage(data));
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
