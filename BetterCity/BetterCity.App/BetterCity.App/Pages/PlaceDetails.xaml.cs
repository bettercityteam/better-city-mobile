﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BetterCity.App.Components;
using BetterCity.App.DI;
using BetterCity.App.Services;
using BetterCity.SharedKernel.Model.Response;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace BetterCity.App.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PlaceDetails : ContentPage
    {
        private readonly ApiClient client;
        private readonly ObservableCollection<ImageSourceContainer> images = new ObservableCollection<ImageSourceContainer>();
        
        public PlaceDetails()
        {
            client = Dependencies.Get<ApiClient>();
            
            InitializeComponent();

            Carousel.IndicatorView = Indicator;
            Carousel.ItemsSource = images;
        }

        public void Load(PlaceInfo place)
        {
            images.Clear();
            
            var imageTasks = place.ImageFiles.Select(async x =>
            {
                var i = await client.GetImage(x);
                var imageSource = ImageSource.FromStream(() => new MemoryStream(i));
                images.Add(new ImageSourceContainer(){Image = imageSource});
            }).ToArray();

            PlaceName.Text = place.Name;
            PlaceRating.Text = place.Rating.ToString();
            PlaceSummary.Text = place.Summary;
            
            Categories.Children.Clear();
            
            foreach (var rating in place.CategoryRatings)
                Categories.Children.Add(new RatingCategoryView(rating.Name, rating.PositiveReviews, rating.Skipped,
                    rating.NegativeReviews));

            // client.GetOwnReviews(place.PlaceId).ContinueWith(r => Dispatcher.BeginInvokeOnMainThread(() =>
            // {
            //     if (r.Status == TaskStatus.RanToCompletion)
            //         Reviews.ItemsSource = r.Result.Select(x => new ReviewContainer(x));
            //     else DisplayAlert("Error", r.Exception?.Message, "Ok");
            // }));
        }

        public class ImageSourceContainer
        {
            public ImageSource Image { get; set; }
        }
    }
}