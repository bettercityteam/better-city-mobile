﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BetterCity.App.DI;
using BetterCity.App.Services;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace BetterCity.App.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Auth : ContentPage
    {
        private readonly Dictionary<string, string> data;
        private readonly ApiClient client;
        private string phoneStripped;

        public Auth(Dictionary<string, string> data)
        {
            this.data = data;
            client = Dependencies.Get<ApiClient>();
            InitializeComponent();
        }

        private void SendCode(object sender, EventArgs e)
        {
            this.phoneStripped = new string(PhoneEntry.Text?.Where(char.IsDigit).ToArray() ?? new char[0]);
            if (string.IsNullOrWhiteSpace(phoneStripped) || phoneStripped.Length < 7)
            {
                ErrorLabel.Text = "Phone is invalid - enter correct number";
                return;
            }

            client.RequestCode(phoneStripped).ContinueWith(r =>
            {
                Dispatcher.BeginInvokeOnMainThread(() =>
                {
                    if (r.Status == TaskStatus.RanToCompletion)
                    {
                        CodeLayout.IsVisible = true;
                    }
                    else ErrorLabel.Text = r.Exception?.Message;
                });
            });
        }

        private void SignIn(object sender, EventArgs e)
        {
            var codeStripped = new string(CodeEntry.Text?.Where(char.IsDigit).ToArray() ?? new char[0]);
            if (string.IsNullOrWhiteSpace(codeStripped) || codeStripped.Length != 4)
            {
                ErrorLabel.Text = "code is incorrect";
                return;
            }

            client.SignIn(phoneStripped, codeStripped).ContinueWith(r => Dispatcher.BeginInvokeOnMainThread(() =>
            {
                if (r.Status == TaskStatus.RanToCompletion)
                {
                    var page = Dependencies.Get<PageSelector>().GetPage(data);
                    Navigation.PushAsync(page);
                }
                else ErrorLabel.Text = r.Exception?.Message;   
            }));
        }

        private void PhoneChanged(object sender, TextChangedEventArgs e)
        {
            ErrorLabel.Text = "";
        }
    }
}