﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BetterCity.App.DAL;
using BetterCity.App.DI;
using BetterCity.App.Model;
using BetterCity.App.Services;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace BetterCity.App.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ReviewRequests : ContentPage
    {
        private readonly ReviewRequest[] requests;
        private readonly PageSelector pageSelector;

        public ReviewRequests()
        {
            InitializeComponent();

            var contextFactory = Dependencies.Get<Func<DataContext>>();
            pageSelector = Dependencies.Get<PageSelector>(); 

            using var context = contextFactory();
            requests = context.Requests.ToArray();

            RequestsView.ItemsSource = requests;
        }

        private void RequestsView_OnItemTapped(object sender, ItemTappedEventArgs e)
        {
            var request = requests[e.ItemIndex];
            var page = pageSelector.GetPage(new Dictionary<string, string>
            {
                [PageSelector.PageSelectionKey] = PageSelector.LaunchReview,
                [PageSelector.RequestId] = request.PlaceId
            });
            Navigation.PushAsync(page);
        }
    }
}