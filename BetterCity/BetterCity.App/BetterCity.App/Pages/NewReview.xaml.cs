﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BetterCity.App.Components;
using BetterCity.App.DAL;
using BetterCity.App.DI;
using BetterCity.App.Services;
using BetterCity.SharedKernel.Model.Response;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace BetterCity.App.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class NewReview : ContentPage
    {
        private readonly string placeId;
        private readonly ApiClient client;
        private readonly Func<DataContext> contextFactory;
        private Queue<ReviewCategory> categories;
        private readonly Queue<CategoryCard> cards = new Queue<CategoryCard>();
        private double step;
        private int leftToText;
        private string reviewText;

        private readonly HashSet<string> likes = new HashSet<string>(), dislikes = new HashSet<string>();

        public NewReview(IReadOnlyDictionary<string, string> data)
        {
            InitializeComponent();

            PrepareCards();

            placeId = data[PageSelector.RequestId];
            client = Dependencies.Get<ApiClient>();
            contextFactory = Dependencies.Get<Func<DataContext>>();
            using var context = contextFactory();
            var request = context.Requests.Find(placeId);
            client.GetReviewCategories(request.PlaceId, request.Waypoints)
                .ContinueWith(r => Dispatcher.BeginInvokeOnMainThread(() =>
                {
                    if (r.Status == TaskStatus.RanToCompletion)
                    {
                        SetVisibility(true);
                        categories = new Queue<ReviewCategory>(r.Result);
                        step = 1d / (categories.Count + 1);
                        leftToText = Convert.ToInt32(Math.Round(categories.Count * 0.4));
                        InitNextCard();
                        return;
                    }

                    DisplayAlert("Error occured", r.Exception?.Message, "Ok");
                }));
        }

        private void PrepareCards()
        {
            foreach (var dir in new[] {SwipeDirection.Left, SwipeDirection.Right, SwipeDirection.Up})
            {
                var swipe = new SwipeGestureRecognizer {Direction = dir};
                swipe.Swiped += OnSwiped;
                Card1.GestureRecognizers.Add(swipe);
                Card2.GestureRecognizers.Add(swipe);
            }

            Card2.TranslationY = -DeviceDisplay.MainDisplayInfo.Height;

            cards.Enqueue(Card1);
            cards.Enqueue(Card2);
        }

        private static bool? GetDecisionFromDirection(SwipeDirection swipeDirection)
        {
            return swipeDirection switch
            {
                SwipeDirection.Right => true,
                SwipeDirection.Left => false,
                _ => (bool?)null
            };
        }

        private void OnSwiped(object sender, SwipedEventArgs e)
        {
            if (!(sender is CategoryCard card)) return;
            if (card.IsPicMode)
                OnDecisionMade(card, card.CategoryId, GetDecisionFromDirection(e.Direction));
            else OnTextSubmit(card, card.GetReviewText());
        }

        private void InitNextCard()
        {
            var nextCard = cards.Dequeue();
            cards.Enqueue(nextCard);

            if (categories.Count > 0)
            {
                if (leftToText == 0)
                {
                    nextCard.LoadForText();
                }
                else
                {
                    var category = categories.Dequeue();
                    nextCard.Load(category.Id, category.Image, category.Name);
                }

                RunFlyIn(nextCard);
            }
            else FinishReview();
        }

        private void SetVisibility(bool isReviewing)
        {
            Card1.IsVisible = Card2.IsVisible = Progress.IsVisible = isReviewing;
            Indicator.IsVisible = !isReviewing;
        }

        private void FinishReview()
        {
            SetVisibility(false);

            client.CreateReview(GetReview()).ContinueWith(r =>
                Dispatcher.BeginInvokeOnMainThread(
                    () =>
                    {
                        if (r.Status == TaskStatus.RanToCompletion)
                        {
                            Navigation.PushAsync(Dependencies.Get<PageSelector>().GetPage(new Dictionary<string, string>()));
                            DisplayAlert("", "Review sent successfully", "Nice!");
                            return;
                        }

                        using var context = contextFactory();
                        // todo : persist review
                        context.SaveChanges();
                    }));
        }

        private SharedKernel.Model.Request.NewReview GetReview()
        {
            return new SharedKernel.Model.Request.NewReview()
            {
                PlaceId = placeId,
                LikedCategories = likes.ToArray(),
                DislikedCategories = dislikes.ToArray(),
                Text = reviewText
            };
        }

        private void RunCardSwipe(CategoryCard card, bool? direction)
        {
            if (direction is null)
            {
                InitNextCard();
                card.TranslateTo(0, -Height, 500, Easing.CubicOut);
            }
            else
            {
                InitNextCard();
                card.TranslateTo(direction.Value ? Width : -Width, 0, 500, Easing.CubicOut)
                    .ContinueWith(_ => Dispatcher.BeginInvokeOnMainThread(() =>
                    {
                        card.TranslationX = 0;
                        card.TranslationY = -Height;
                    }));
            }
        }

        private void RunFlyIn(CategoryCard card)
        {
            card.TranslationY = -Height;
            card.TranslateTo(0, 0, 500, Easing.CubicOut);
        }

        private void OnDecisionMade(CategoryCard card, string categoryId, bool? decision)
        {
            leftToText--;
            if (decision == true) likes.Add(categoryId);
            else if (decision == false) dislikes.Add(categoryId);

            Progress.Progress += step;

            RunCardSwipe(card, decision);
        }

        private void OnTextSubmit(CategoryCard card, string obj)
        {
            leftToText--;
            Progress.Progress += step;
            reviewText = obj;
            RunCardSwipe(card, true);
        }
    }
}