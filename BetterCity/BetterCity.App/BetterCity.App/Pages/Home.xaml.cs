﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using BetterCity.App.DI;
using BetterCity.App.Helpers;
using BetterCity.App.Model;
using BetterCity.App.Services;
using BetterCity.SharedKernel;
using BetterCity.SharedKernel.Model.Response;
using Xamarin.Forms;
using Xamarin.Forms.Maps;
using Xamarin.Forms.Xaml;

namespace BetterCity.App.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Home : TabbedPage
    {
        private readonly ApiClient client;
        private readonly Dictionary<string, PlaceGeometry> loadedGeometry = new Dictionary<string, PlaceGeometry>();
        private readonly Dictionary<string, PlaceInfo> loadedPlaces = new Dictionary<string, PlaceInfo>();

        private static Color InterpolateRating(double value)
        {
            return new Color(255 * (1 - value), 255 * value, 0, 70);
        }

        public Home()
        {
            client = Dependencies.Get<ApiClient>();

            InitializeComponent();

            PreparePage();
        }

        private async void PreparePage()
        {
            try
            {
                await Task.Run(() =>
                {
                    while (Map.VisibleRegion is null) ;
                });
                await RequestAndDrawPlaces();
            }
            catch (Exception e)
            {
                // ignored
            }
        }

        private async Task RequestAndDrawPlaces()
        {
            var center = Map.VisibleRegion.Center;
            var placeTuples = await RequestAndCachePlaces(center);
            var polys = placeTuples
                .Select(p =>
                {
                    var (info, geometry) = p;
                    var polygon = new Polygon()
                    {
                        StrokeWidth = 2,
                        StrokeColor = InterpolateRating(info.Rating),
                        FillColor = InterpolateRating(info.Rating),
                    };
                    foreach (var position in geometry.BoundingPolygon.Select(x =>
                        new Position(x.Latitude, x.Longitude)))
                        polygon.Geopath.Add(position);
                    return polygon;
                });
            Dispatcher.BeginInvokeOnMainThread(() =>
            {
                foreach (var poly in polys) Map.MapElements.Add(poly);
            });
        }

        private async Task<IEnumerable<(PlaceInfo info, PlaceGeometry geometry)>> RequestAndCachePlaces(Position center)
        {
            var placeGeometries = await client.GetPlacesInArea(center.Latitude, center.Longitude,
                Map.VisibleRegion.Radius.Kilometers);
            var places = await Task.WhenAll(placeGeometries.Select(x => client.GetPlace(x.PlaceId)));
            foreach (var place in places) loadedPlaces[place.PlaceId] = place;
            foreach (var geometry in placeGeometries) loadedGeometry[geometry.PlaceId] = geometry;
            var placeTuples = places.Join(placeGeometries, pi => pi.PlaceId, g => g.PlaceId, (info, geometry) => (info, geometry));
            return placeTuples;
        }

        private void OnMapClicked(object sender, MapClickedEventArgs e)
        {
            var point = new RoutePoint(e.Position.Latitude, e.Position.Longitude, DateTimeOffset.Now);
            var placeId = loadedGeometry.Values.FirstOrDefault(x => GeometryRoutines.IsWithin(x.BoundingPolygon)(point))?.PlaceId;
            if (!string.IsNullOrWhiteSpace(placeId))
            {
                var place = loadedPlaces[placeId];
                Details.Load(place);
                CurrentPage = Details;
            }
            else
            {
                DisplayAlert("Wait a bit", "Places are loading", "Ok");
                var _ = RequestAndCachePlaces(Map.VisibleRegion.Center);
            }
        }

        private void OnMapPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(Map.VisibleRegion))
            {
                var _ = RequestAndCachePlaces(Map.VisibleRegion.Center);
            }
            // throw new NotImplementedException();
        }
    }
}