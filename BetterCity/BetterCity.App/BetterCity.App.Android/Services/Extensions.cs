﻿using System.Collections.Generic;
using System.Linq;
using Android.OS;

namespace BetterCity.App.Droid.Services
{
    public static class Extensions
    {
        public static Dictionary<string, string> ToDict(this Bundle bundle)
        {
            if (bundle is null) return new Dictionary<string, string>();
            return bundle.KeySet()?.ToDictionary(x => x, x => bundle.Get(x)?.ToString() ?? "") ?? new Dictionary<string, string>();
        }
    }
}