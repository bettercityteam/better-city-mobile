﻿using System.Collections.Generic;
using System.Linq;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Widget;
using AndroidX.Core.App;
using BetterCity.App.Droid.Tracking;
using BetterCity.App.Model;
using BetterCity.App.Services;
using Java.Lang;
using Xamarin.Forms.Internals;
using NotificationCompat = AndroidX.Media.App.NotificationCompat;

namespace BetterCity.App.Droid.Services
{
    public class NotificationSender
    {
        private const string ChannelId = "1762849";
        public const string ExtraDelay = "delayFor";

        private const int codeOpen = 1,
            codeDelay3Min = 2,
            codeDelay15Min = 3,
            codeDelay1Hour = 4,
            codeDelay6Hours = 5,
            notificationId = 124901;

        private readonly ContextWrapper contextWrapper;
        private readonly NotificationManager notificationManager;
        private NotificationSender(ContextWrapper contextWrapper, NotificationManager notificationManager)
        {
            this.contextWrapper = contextWrapper;
            this.notificationManager = notificationManager;

            if (Build.VERSION.SdkInt >= BuildVersionCodes.O
                && notificationManager!.NotificationChannels!.All(x => x.Id != ChannelId))
            {
                var channel = new NotificationChannel(ChannelId, "Better City notifications", NotificationImportance.Default)
                {
                    Description = "Visited places review request notifications"
                };
                notificationManager.CreateNotificationChannel(channel);
            }
        }
        
        public static NotificationSender? For(ContextWrapper wrapper)
        {
            var service = wrapper.GetSystemService(Context.NotificationService);
            if (service is NotificationManager nm)
            {
                return new NotificationSender(wrapper, nm);
            }
            return null;
        }
        
        public Exception? SendNotificationFor(ReviewRequest[] requests)
        {
            if (requests.Length == 0) return null;
            var id = requests[0].PlaceId;
            var launchIntent = new Intent(contextWrapper, typeof(MainActivity));
            launchIntent.PutExtra(PageSelector.PageSelectionKey, PageSelector.LaunchReview);
            var intent = PendingIntent.GetActivity(contextWrapper, codeOpen, launchIntent,
                PendingIntentFlags.UpdateCurrent);
            try
            {
                var builder = (Build.VERSION.SdkInt >= BuildVersionCodes.O
                    ? new AndroidX.Core.App.NotificationCompat.Builder(contextWrapper, ChannelId)
                    : new AndroidX.Core.App.NotificationCompat.Builder(contextWrapper))
                    .SetSmallIcon(Resource.Drawable.notification_icon_background)
                    .SetContentTitle("Оставьте отзыв!")
                    .SetContent(GetBigView(requests))
                    .SetContentIntent(intent)
                    .SetDeleteIntent(CreateDelayIntent(3, codeDelay3Min, id));
                notificationManager.Notify(notificationId, builder.Build());
                return null;
            }
            catch (Exception ex)
            {
                return ex;
            }
        }

        private string BuildNotificationContent(ReviewRequest[] rs)
        {
            object data = rs.Length == 1 ? (object)rs[0].Name : rs.Length;
            return string.Format(rs.Length == 1 
                ? "Спасибо, что посетили {0}! Пожалуйста, оставьте отзыв - это займёт 5 минут!" 
                : "Спасибо! Вы посетили уже {1} мест! Не забудьте оставить отзывы!", 
                data);
        }
        
        private RemoteViews GetBigView(ReviewRequest[] requests)
        {
            var id = requests[0].PlaceId;
            var view = new RemoteViews(contextWrapper.PackageName, Resource.Layout.newRequestSmall);
            view.SetTextViewText(Resource.Id.notificationContent, BuildNotificationContent(requests));
            view.SetOnClickPendingIntent(Resource.Id.delay15, CreateDelayIntent(15, codeDelay15Min, id));
            view.SetOnClickPendingIntent(Resource.Id.delay60, CreateDelayIntent(60, codeDelay1Hour, id));
            view.SetOnClickPendingIntent(Resource.Id.delay360, CreateDelayIntent(360, codeDelay6Hours, id));
            return view;
        }

        private PendingIntent CreateDelayIntent(int minutes, int requestCode, string requestId)
        {
            var baseContext = contextWrapper.BaseContext;
            var intent = new Intent(baseContext, typeof(LocationTrackingService));
            intent.PutExtra(ExtraDelay, minutes);
            intent.PutExtra(PageSelector.RequestId, requestId);
            return PendingIntent.GetService(baseContext, requestCode, intent, 0);
        }

        public void Cancel()
        {
            notificationManager.Cancel(notificationId);
        }
    }
}