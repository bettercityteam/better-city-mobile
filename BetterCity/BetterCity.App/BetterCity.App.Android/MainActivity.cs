﻿using System;
using Android.App;
using Android.Content.PM;
using Android.Runtime;
using Android.OS;
using Android;
using AndroidX.Core.App;
using BetterCity.App.DI;
using BetterCity.App.Droid.DI;
using BetterCity.App.Droid.Services;
using BetterCity.App.Droid.Tracking;
using Xamd.ImageCarousel.Forms.Plugin.Droid;

namespace BetterCity.App.Droid
{
    [Activity(Label = "BetterCity.App", Icon = "@mipmap/icon", Theme = "@style/MainTheme", MainLauncher = true,
        ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation | ConfigChanges.UiMode |
                               ConfigChanges.ScreenLayout | ConfigChanges.SmallestScreenSize)]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            Dependencies.Register(DeviceSpecificDi.RegisterServices);

            base.OnCreate(savedInstanceState);

            Xamarin.Essentials.Platform.Init(this, savedInstanceState);
            global::Xamarin.Forms.Forms.Init(this, savedInstanceState);
            Xamarin.FormsMaps.Init(this, savedInstanceState);
            ImageCarouselRenderer.Init();

            StartService(new Android.Content.Intent(this, typeof(Tracking.LocationTrackingService)));

            if (Intent?.Extras?.ContainsKey(LocationTrackingService.extraNotificationId) == true)
            {
                var notificationId = Intent.GetIntExtra(LocationTrackingService.extraNotificationId, 0);
                var manager = (NotificationManager) GetSystemService(NotificationService);
                manager?.Cancel(notificationId);
            }

            LoadApplication(new App(Intent?.Extras?.ToDict()));
        }

        public override void OnRequestPermissionsResult(int requestCode, string[] permissions,
            [GeneratedEnum] Android.Content.PM.Permission[] grantResults)
        {
            Xamarin.Essentials.Platform.OnRequestPermissionsResult(requestCode, permissions, grantResults);

            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }

        protected override void OnStart()
        {
            base.OnStart();
            if ((int) Build.VERSION.SdkInt < 23) return;
            if (CheckSelfPermission(Manifest.Permission.AccessFineLocation) != Permission.Granted)
                RequestPermissions(
                    new[] {Manifest.Permission.AccessCoarseLocation, Manifest.Permission.AccessFineLocation}, 123);
        }
    }
}