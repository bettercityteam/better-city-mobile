﻿using Android.App;
using Android.Content;
using Android.Gms.Common;
using Android.Gms.Location;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using BetterCity.App.DAL;
using BetterCity.App.DI;
using BetterCity.App.Model;
using BetterCity.App.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using AndroidX.Core.App;
using BetterCity.App.Droid.Services;
using BetterCity.SharedKernel.Model.Response;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Xamarin.Essentials;

namespace BetterCity.App.Droid.Tracking
{
    [Service]
    public class LocationTrackingService : Service
    {
        private const uint
            mask3min = 0x0,
            mask15min = 0x20_00_00_00,
            mask1hour = 0x40_00_00_00,
            mask6hours = 0x60_00_00_00,
            maskOpenReview = 0x80_00_00_00;

        public const string
            extraDelay = "delayFor",
            extraNotificationId = "notificationCode";
        
        private Timer pollTimer, notificationTimer;
        private CancellationTokenSource tokenSource = new CancellationTokenSource();
        private RoutePointsStore pointsStore;
        private AppSettings settings;
        private VisitedPlaceResolver resolver;
        private Func<DataContext> contextFactory;

        [return: GeneratedEnum]
        public override StartCommandResult OnStartCommand(Intent intent, [GeneratedEnum] StartCommandFlags flags,
            int startId)
        {
            Init();

            if (intent?.Extras?.ContainsKey(extraDelay) == true)
            {
                var delayFor = intent.GetIntExtra(extraDelay, 0);
                var requestId = intent.GetStringExtra(PageSelector.RequestId);
                if (delayFor != 0 && !string.IsNullOrWhiteSpace(requestId))
                {
                    using var context = contextFactory();
                    var request = context.Requests.Find(requestId);
                    if (!(request is null))
                    {
                        request.MutedTill = DateTimeOffset.Now.AddMinutes(delayFor);
                        context.Requests.Update(request);
                        context.SaveChanges();
                    }

                    NotificationSender.For(this).Cancel();
                }
            }

            if (pollTimer is null && GoogleServicesPresent())
                pollTimer = new Timer(PollLocation, null, TimeSpan.Zero,
                    TimeSpan.FromSeconds(settings.PollIntervalSeconds));
            notificationTimer ??= new Timer(CheckNotifications, null, TimeSpan.Zero, TimeSpan.FromMinutes(1));

            return StartCommandResult.Sticky;
        }

        private void Init()
        {
            pointsStore ??= Dependencies.Get<RoutePointsStore>();
            settings ??= Dependencies.Get<AppSettings>();
            contextFactory ??= Dependencies.Get<Func<DataContext>>();

            if (resolver is null)
            {
                resolver = Dependencies.Get<VisitedPlaceResolver>();
                resolver.PlacesFound += OnPlacesFound;
            }
        }

        private void OnPlacesFound((PlaceGeometry, Waypoint[])[] places)
        {
            var requests = places.Select(ReviewRequest.FromPlaceAndWaypoints).ToArray();
            UpsertRequests(requests);
            PlaceNotifications(requests);
        }

        private void PlaceNotifications(IEnumerable<ReviewRequest> requests)
        {
            var now = DateTimeOffset.Now;
            var ex = NotificationSender.For(this)?
                .SendNotificationFor(requests.Where(x => x.MutedTill < now).ToArray());
            if (!(ex is null))
            {
                Log.Warn($"{nameof(BetterCity)}.{nameof(LocationTrackingService)}.{nameof(PollLocation)}",
                    ex.ToString());
            }
        }

        private void UpsertRequests(ReviewRequest[] requests)
        {
            try
            {
                using var context = contextFactory();
                foreach (var request in requests)
                {
                    if (context.Requests.Find(request.PlaceId) is null) context.Requests.Add(request);
                }

                context.SaveChanges();
            }
            catch (DbUpdateException ex)
            {
                Log.Warn($"{nameof(BetterCity)}.{nameof(LocationTrackingService)}.{nameof(PollLocation)}",
                    ex.ToString());
            }
        }

        private bool GoogleServicesPresent()
        {
            var instance = GoogleApiAvailability.Instance;
            var result = instance.IsGooglePlayServicesAvailable(this);
            if (result == ConnectionResult.Success) return true;
            if (instance.IsUserResolvableError(result))
                Toast.MakeText(this, instance.GetErrorString(result), ToastLength.Long);
            return false;
        }

        private async void PollLocation(object _)
        {
            try
            {
                var result = await Geolocation.GetLocationAsync(new GeolocationRequest(GeolocationAccuracy.Medium),
                    tokenSource.Token);
                pointsStore.AddPoint(result.Latitude, result.Longitude, result.Timestamp);
            }
            catch (Exception ex)
            {
                Log.Warn($"{nameof(BetterCity)}.{nameof(LocationTrackingService)}.{nameof(PollLocation)}",
                    ex.ToString());
            }
        }

        private void CheckNotifications(object _)
        {
            using var context = contextFactory();
            var requests = context.Requests.ToList();
            PlaceNotifications(requests);
        }

        public override IBinder OnBind(Intent intent)
        {
            throw new NotImplementedException();
        }

        public override void OnDestroy()
        {
            tokenSource.Cancel();
            pollTimer.Change(Timeout.Infinite, 1);
            notificationTimer.Change(Timeout.Infinite, 1);
            base.OnDestroy();
        }
    }
}